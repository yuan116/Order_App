﻿using SQLite;

namespace Order_App.model
{
    public class OrderModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public double Subtotal { get; set; }
        public string Status { get; set; }
    }
}
