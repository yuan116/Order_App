﻿using SQLite;
using System;
using System.IO;

namespace Order_App.model
{
    public class Database
    {
        private static string DBPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "OrderApp.db3");
        public static SQLiteConnection db = new SQLiteConnection(DBPath);
    }
}
