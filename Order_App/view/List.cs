﻿using System;
using Xamarin.Forms;

namespace Order_App.view
{
    public class List : ContentPage
    {
        //Declaration of Dish List, Price
        private string[] DishName = { "Nasi Lemak", "Nasi Goreng", "Char Kuey Teow", "Mee Goreng", "Mee Sup", "Pepperoni Pizza", "Skewered Grilled Cheese Turkey Bacon", "Korean Egg Toast with Mozzarella Cheese", "Garlic Fried Rice", "Salted Caramel Popcorn" };
        private double[] DishPrice = { 3, 4, 6, 3.5, 6, 20, 7, 10, 8, 5 };

        //Declaration of Drink List, Price
        private string[] DrinkName = { "100 Plus", "Coca-Cola", "Coke-Zero", "Milo Ice", "Hot Coffee", "Ice Lemon Tea", "Apple Juice", "Orange Juice", "Watermelon Juice", "Sky Juice" };
        private double[] DrinkPrice = { 3, 3, 2, 3, 3, 3, 4, 4, 5, 2 };

        //Declaration of Dessert List, Price
        private string[] DessertName = { "Vanilla", "Chocolate", "Banana Boat" };
        private double[] DessertPrice = { 2, 3, 5 };

        //Declaration of Snack List, Price
        private string[] SnackName = { "French Fries", "Cheesy Wedges" };
        private double[] SnackPrice = { 6, 7.5 };

        //Declare StackLayout
        private StackLayout[] MenuLayout;

        //To differentiate the menu when user clicked on layout
        private string[] MenuName;
        private double[] MenuPrice;
        private int count, menu_length;

        public List(string type)
        {
            //Set the type of menu choose by user
            if(type == "Dish")
            {
                MenuName = DishName;
                MenuPrice = DishPrice;
            }
            else if(type == "Drink")
            {
                MenuName = DrinkName;
                MenuPrice = DrinkPrice;
            }
            else if(type == "Dessert")
            {
                MenuName = DessertName;
                MenuPrice = DessertPrice;
            }
            else if (type == "Snack")
            {
                MenuName = SnackName;
                MenuPrice = SnackPrice;
            }
            menu_length = MenuName.Length;

            //Write menu name into label
            Label[] MenuNameLabel = new Label[menu_length];
            for(count = 0; count < menu_length; count++)
            {
                MenuNameLabel[count] = new Label
                {
                    FontSize = 20,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment = TextAlignment.Center,
                    Text = MenuName[count],
                    TextColor = Color.White
                };
            }

            //Write menu price into label
            Label[] MenuPriceLabel = new Label[menu_length];
            for(count = 0; count < menu_length; count++)
            {
                MenuPriceLabel[count] = new Label
                {
                    FontSize = 15,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment = TextAlignment.Center,
                    Text = $"RM {MenuPrice[count]:F2}",
                    TextColor = Color.White
                };
            }

            //Declare and create event handling for stack layout
            TapGestureRecognizer LayoutTapGesture = new TapGestureRecognizer();
            LayoutTapGesture.Tapped += MenuClicked;

            //Put name and price in stack layout
            MenuLayout = new StackLayout[menu_length];
            for(count = 0; count < menu_length; count++)
            {
                MenuLayout[count] = new StackLayout
                {
                    BackgroundColor = Color.FromHex("#03a1fc"),
                    Children = { MenuNameLabel[count], MenuPriceLabel[count] },
                    //Add event handling
                    GestureRecognizers = { LayoutTapGesture }
                };
            }

            //Declare and create table
            Grid BodyContent = new Grid
            {
                Padding = 15,
                ColumnDefinitions =
                {
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) }
                }
            };

            //Table's row definitions
            for(count = 0; count < (menu_length / 2) + 1; count++)
            {
                BodyContent.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) } );
            }

            //Add stack layout into table
            count = 0;
            for(int row = 0; row < (menu_length / 2) + 1; row++)
            {
                for(int col = 0; col < 2; col++)
                {
                    if(count < menu_length)
                    {
                        BodyContent.Children.Add(MenuLayout[count], col, row);
                        count++;
                    }
                }
            }

            //Set Page
            Title = "Order App";
            Padding = 10;
            Content = new ScrollView
            {
                Content = new StackLayout
                {
                    Children = { BodyContent }
                }
            };
        }

        //Go to Add Order Page by detect which menu is being clicked
        private void MenuClicked(object sender, EventArgs evt)
        {
            for(count = 0; count < menu_length; count++)
            {
                if(sender == MenuLayout[count])
                {
                    break;
                }
            }

            Navigation.PushAsync(new AddOrder(MenuName[count], MenuPrice[count]));
        }
    }
}