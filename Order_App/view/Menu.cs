﻿using System;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace Order_App.view
{
    public class Menu : ContentPage
    {
        //Declaration of Frame
        private Frame DishFrame, DrinkFrame, DessertFrame, SnackFrame, CheckOrderListFrame;

        public Menu()
        {
            //Declaration and creation of View for Dish
            Image DishImage = new Image{ Source = "dish.png" };
            Label DishLabel = new Label
            {
                FontAttributes = FontAttributes.Bold,
                FontSize = 20,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                Text = "Food List"
            };
            Grid DishGrid = new Grid
            {
                ColumnDefinitions =
                {
                    new ColumnDefinition{ Width = GridLength.Auto },
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition{ Height = GridLength.Auto }
                }
            };
            DishGrid.Children.Add(DishImage, 0, 0);
            DishGrid.Children.Add(DishLabel, 1, 3, 0, 1);
            DishFrame = new Frame{ Content = DishGrid };

            //Drink
            Image DrinkImage = new Image{ Source = "drink.png" };
            Label DrinkLabel = new Label
            {
                FontAttributes = FontAttributes.Bold,
                FontSize = 20,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                Text = "Drink List"
            };
            Grid DrinkGrid = new Grid
            {
                ColumnDefinitions =
                {
                    new ColumnDefinition{ Width = GridLength.Auto },
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition{ Height = GridLength.Auto }
                }
            };
            DrinkGrid.Children.Add(DrinkImage, 0, 0);
            DrinkGrid.Children.Add(DrinkLabel, 1, 3, 0, 1);
            DrinkFrame = new Frame { Content = DrinkGrid };

            //Dessert
            Image DessertImage = new Image { Source = "dessert.png" };
            Label DessertLabel = new Label
            {
                FontAttributes = FontAttributes.Bold,
                FontSize = 20,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                Text = "Dessert List"
            };
            Grid DessertGrid = new Grid
            {
                ColumnDefinitions =
                {
                    new ColumnDefinition{ Width = GridLength.Auto },
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition{ Height = GridLength.Auto }
                }
            };
            DessertGrid.Children.Add(DessertImage, 0, 0);
            DessertGrid.Children.Add(DessertLabel, 1, 3, 0, 1);
            DessertFrame = new Frame { Content = DessertGrid };

            //Dessert
            Image SnackImage = new Image { Source = "snack.png" };
            Label SnackLabel = new Label
            {
                FontAttributes = FontAttributes.Bold,
                FontSize = 20,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                Text = "Snack List"
            };
            Grid SnackGrid = new Grid
            {
                ColumnDefinitions =
                {
                    new ColumnDefinition{ Width = GridLength.Auto },
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition{ Height = GridLength.Auto }
                }
            };
            SnackGrid.Children.Add(SnackImage, 0, 0);
            SnackGrid.Children.Add(SnackLabel, 1, 3, 0, 1);
            SnackFrame = new Frame { Content = SnackGrid };

            //Center Content for Dish and Drink
            StackLayout CenterContent = new StackLayout
            {
                HeightRequest = DeviceDisplay.MainDisplayInfo.Height / 2,
                Padding = 20,
                Children = { DishFrame, DrinkFrame, DessertFrame, SnackFrame }
            };

            //Check Order List
            Label CheckOrderListLabel = new Label
            {
                FontSize = 20,
                HorizontalOptions = LayoutOptions.Center,
                Text = "Check Order List",
                TextColor = Color.White
            };
            CheckOrderListFrame = new Frame
            {
                BackgroundColor = Color.FromHex("#03a1fc"),
                Content = CheckOrderListLabel,
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };            

            //Event Handling for frame
            TapGestureRecognizer FrameTapGesture = new TapGestureRecognizer();
            FrameTapGesture.Tapped += FrameClicked;
            DishFrame.GestureRecognizers.Add(FrameTapGesture);
            DrinkFrame.GestureRecognizers.Add(FrameTapGesture);
            DessertFrame.GestureRecognizers.Add(FrameTapGesture);
            SnackFrame.GestureRecognizers.Add(FrameTapGesture);
            CheckOrderListFrame.GestureRecognizers.Add(FrameTapGesture);

            //Set Page
            Title = "Order App";
            NavigationPage.SetHasNavigationBar(this, true);    
            Content = new StackLayout
            {
                Children = { CenterContent, CheckOrderListFrame }
            };
        }

        //Go to new page by detect which frame is being clicked
        private void FrameClicked(object sender, EventArgs evt)
        {
            if(sender == DishFrame)
            {
                Navigation.PushAsync(new List("Dish"));
            }
            else if(sender == DrinkFrame)
            {
                Navigation.PushAsync(new List("Drink"));
            }
            else if(sender == DessertFrame)
            {
                Navigation.PushAsync(new List("Dessert"));
            }
            else if(sender == SnackFrame)
            {
                Navigation.PushAsync(new List("Snack"));
            }
            else if(sender == CheckOrderListFrame)
            {
                Navigation.PushAsync(new CheckOrderList());
            }
        }
    }
}