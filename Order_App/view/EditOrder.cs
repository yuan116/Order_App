﻿using Order_App.model;
using System;
using Xamarin.Forms;

namespace Order_App.view
{
    public class EditOrder : ContentPage
    {
        //Declaration of View
        private Button BackButton, EditButton;
        private Label QuantityLabel, SubTotalLabel;
        private Stepper QuantityStepper;

        //Model
        private OrderModel OrderObj = new OrderModel();

        public EditOrder(OrderModel OrderObj)
        {
            this.OrderObj = OrderObj;

            Label NameLabel = new Label
            {
                FontSize = 30,
                VerticalOptions = LayoutOptions.Center,
                Text = $"Name : {OrderObj.Name}"
            };

            Label PriceLabel = new Label
            {
                FontSize = 30,
                VerticalOptions = LayoutOptions.Center,
                Text = $"Price : RM {OrderObj.Price:F2}"
            };

            QuantityLabel = new Label
            {
                FontSize = 30,
                VerticalOptions = LayoutOptions.Center,
                Text = $"Quantity : {OrderObj.Quantity}"
            };

            QuantityStepper = new Stepper
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Minimum = 1,
                Value = OrderObj.Quantity
            };

            SubTotalLabel = new Label
            {
                FontSize = 30,
                VerticalOptions = LayoutOptions.Center,
                Text = $"SubTotal : RM {OrderObj.Subtotal:F2}"
            };

            BackButton = new Button
            {
                BackgroundColor = Color.FromHex("#03a1fc"),
                FontSize = 20,
                TextColor = Color.White,
                Text = "Back To List",
            };

            EditButton = new Button
            {
                BackgroundColor = Color.FromHex("#03a1fc"),
                FontSize = 20,
                TextColor = Color.White,
                Text = "Edit Order",
            };

            Grid BodyContent = new Grid
            {
                ColumnDefinitions =
                {
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition{ Height = new GridLength(70, GridUnitType.Absolute) },
                    new RowDefinition{ Height = new GridLength(70, GridUnitType.Absolute) },
                    new RowDefinition{ Height = new GridLength(70, GridUnitType.Absolute) },
                    new RowDefinition{ Height = new GridLength(70, GridUnitType.Absolute) },
                    new RowDefinition{ Height = GridLength.Auto }
                }
            };

            BodyContent.Children.Add(NameLabel, 0, 2, 0, 1);
            BodyContent.Children.Add(PriceLabel, 0, 2, 1, 2);
            BodyContent.Children.Add(QuantityLabel, 0, 2);
            BodyContent.Children.Add(QuantityStepper, 1, 2);
            BodyContent.Children.Add(SubTotalLabel, 0, 2, 3, 4);
            BodyContent.Children.Add(BackButton, 0, 4);
            BodyContent.Children.Add(EditButton, 1, 4);

            //Event Handling
            QuantityStepper.ValueChanged += StepperClicked;
            BackButton.Clicked += ButtonClicked;
            EditButton.Clicked += ButtonClicked;

            //Set Page
            Title = "Order App";
            Padding = 10;
            Content = BodyContent;
        }

        private void StepperClicked(object sender, ValueChangedEventArgs evt)
        {
            QuantityLabel.Text = "Quantity : " + evt.NewValue;
            SubTotalLabel.Text = $"SubTotal : RM {OrderObj.Price * evt.NewValue:F2}";
        }

        private void ButtonClicked(object sender, EventArgs e)
        {
            if (sender == BackButton)
            {
                Navigation.PopAsync();
            }
            else if (sender == EditButton)
            {
                OrderObj.Quantity = Convert.ToInt32(QuantityStepper.Value);
                OrderObj.Subtotal = OrderObj.Price * OrderObj.Quantity;

                Database.db.Update(OrderObj);

                Navigation.PopAsync();
                Navigation.PopAsync();
                Navigation.PushAsync(new CheckOrderList());
            }
        }
    }
}