﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Order_App.view
{
    public class Index : ContentPage
    {
        //Declaration of Label
        private Label BlinkLabel, LoadingLabel;

        public Index()
        {
            Label AppTitleLabel = new Label
            {
                FontAttributes = FontAttributes.Bold,
                FontSize = 100,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Color.FromHex("#03a1fc"),
                Text = "Order App"
            };

            BlinkLabel = new Label
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                FontSize = 20
            };
            Blink();

            //create 2 rows 1 column
            Grid GridContainer = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition{ Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition{ Height = new GridLength(1, GridUnitType.Star) },
                },
                ColumnDefinitions =
                {
                    new ColumnDefinition{ Width = GridLength.Auto }
                }
            };
            GridContainer.Children.Add(AppTitleLabel, 0, 0);
            GridContainer.Children.Add(BlinkLabel, 0, 1);

            Frame Body = new Frame{ Content = GridContainer };

            //Add TapGestureRecognizer, it allow frame to run EventHandling
            TapGestureRecognizer TapGesture = new TapGestureRecognizer();
            TapGesture.Tapped += FrameClicked;
            Body.GestureRecognizers.Add(TapGesture);

            //Set page
            Content = Body;
            NavigationPage.SetHasNavigationBar(this, false);
        }

        //Click on frame will go to loading screen
        private void FrameClicked(object sender, EventArgs evt)
        {
            Image CircleImage = new Image
            {
                Source = "circle.png",
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            LoadingLabel = new Label
            {
                FontAttributes = FontAttributes.Bold,
                FontSize = 20,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                TextColor = Color.Orange
            };

            RelativeLayout RelativeContainer = new RelativeLayout();
            RelativeContainer.Children.Add
            (
                CircleImage,
                Constraint.Constant(0),
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) => { return parent.Width; }),
                Constraint.RelativeToParent((parent) => { return parent.Height; })
            );
            RelativeContainer.Children.Add
            (
                LoadingLabel,
                Constraint.Constant(0),
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) => { return parent.Width; }),
                Constraint.RelativeToParent((parent) => { return parent.Height; })
            );

            Content = RelativeContainer;

            LoadingScreen();
        }

        //To make the label blink for every 0.5 seconds
        private async void Blink()
        {
            while(true)
            {
                await Task.Delay(500);
                BlinkLabel.Text = "";
                await Task.Delay(500);
                BlinkLabel.Text = "Click Anywhere To Continue";
            }
        }

        //Loading screen and go to next page
        private async void LoadingScreen()
        {
            int count = 0;
            while(count < 10)
            {
                await Task.Delay(200);
                LoadingLabel.Text = "Loading";
                count++;
                await Task.Delay(200);
                LoadingLabel.Text = "Loading..";
                count++;
                await Task.Delay(200);
                LoadingLabel.Text = "Loading....";
                count++;
                await Task.Delay(200);
                LoadingLabel.Text = "Loading......";
                count++;
            }

            //Add Menu as Root Page and remove Index Page in Navigation
            Navigation.InsertPageBefore(new Menu(), this);
            await Navigation.PopToRootAsync();
        }
    }
}