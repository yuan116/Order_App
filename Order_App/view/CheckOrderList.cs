﻿using Order_App.model;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace Order_App.view
{
    public class CheckOrderList : ContentPage
    {
        private List<OrderModel> OrderList;

        //Declaration of View
        private ImageButton[] EditButton, DeleteButton;
        private StackLayout[] ButtonLayout;

        private int count, order_length;

        public CheckOrderList()
        {
            RefreshContent();

            //Set page
            Title = "Order App";
            Padding = 10;
        }

        //Refresh the content after delete an order from list
        public void RefreshContent()
        {
            OrderList = GetPendingOrderList();
            order_length = OrderList.Count;

            if(order_length == 0)
            {
                Content = new Label
                {
                    FontSize = 30,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment = TextAlignment.Center,
                    Text = "No Order has been added"
                };
            }
            else
            {
                Label NumberLabel = new Label { Text = "No." };
                Label NameLabel = new Label { HorizontalTextAlignment = TextAlignment.Center, Text = "Name" };
                Label PriceLabel = new Label { HorizontalTextAlignment = TextAlignment.Center, Text = "Price" };
                Label QuantityLabel = new Label { HorizontalTextAlignment = TextAlignment.Center, Text = "Quantity" };
                Label SubTotalLabel = new Label { HorizontalTextAlignment = TextAlignment.Center, Text = "SubTotal" };
                Label ActionLabel = new Label { HorizontalTextAlignment = TextAlignment.Center, Text = "Action" };

                //Declare and create a table
                Grid BodyContent = new Grid
                {
                    HeightRequest = DeviceDisplay.MainDisplayInfo.Height,
                    ColumnDefinitions =
                    {
                        new ColumnDefinition{ Width = GridLength.Auto },
                        new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) },
                        new ColumnDefinition{ Width = GridLength.Auto },
                        new ColumnDefinition{ Width = GridLength.Auto },
                        new ColumnDefinition{ Width = GridLength.Auto },
                        new ColumnDefinition{ Width = GridLength.Auto }
                    },
                    RowDefinitions =
                    {
                        new RowDefinition{ Height = GridLength.Auto }
                    }
                };

                //Declare and create Label
                Label[] AllNumberLabel = new Label[order_length];
                Label[] AllNameLabel = new Label[order_length];
                Label[] AllPriceLabel = new Label[order_length];
                Label[] AllQuantityLabel = new Label[order_length];
                Label[] AllSubTotalLabel = new Label[order_length];
                EditButton = new ImageButton[order_length];
                DeleteButton = new ImageButton[order_length];
                ButtonLayout = new StackLayout[order_length];

                //Table's row definitions and calculate total price
                double TotalPrice = 0;
                for(count = 0; count < order_length; count++)
                {
                    BodyContent.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                    TotalPrice += OrderList[count].Subtotal;

                    //Insert the attribute from OrderList to each View respectively
                    AllNumberLabel[count] = new Label { HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, Text = $"{count + 1}" };
                    AllNameLabel[count] = new Label { VerticalTextAlignment = TextAlignment.Center, Text = OrderList[count].Name };
                    AllPriceLabel[count] = new Label { HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, Text = $"{OrderList[count].Price:F2}" };
                    AllQuantityLabel[count] = new Label { HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, Text = $"{OrderList[count].Quantity}" };
                    AllSubTotalLabel[count] = new Label { HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Center, Text = $"{OrderList[count].Subtotal:F2}" };
                    EditButton[count] = new ImageButton { BackgroundColor = Color.FromHex("#03a1fc"), HeightRequest = 30, WidthRequest = 30, Source = "edit.png" };
                    DeleteButton[count] = new ImageButton { BackgroundColor = Color.Red, HeightRequest = 30, WidthRequest = 30, Source = "delete.png" };
                    ButtonLayout[count] = new StackLayout { Orientation = StackOrientation.Horizontal, Children = { EditButton[count], DeleteButton[count] } };

                    //Add Event Handling for Buttons
                    EditButton[count].Clicked += EditButtonClicked;
                    DeleteButton[count].Clicked += DeleteButtonClicked;
                }

                //Insert view into table
                BodyContent.Children.Add(NumberLabel, 0, 0);
                BodyContent.Children.Add(NameLabel, 1, 0);
                BodyContent.Children.Add(PriceLabel, 2, 0);
                BodyContent.Children.Add(QuantityLabel, 3, 0);
                BodyContent.Children.Add(SubTotalLabel, 4, 0);
                BodyContent.Children.Add(ActionLabel, 5, 0);

                for(count = 0; count < order_length; count++)
                {
                    BodyContent.Children.Add(AllNumberLabel[count], 0, count + 1);
                    BodyContent.Children.Add(AllNameLabel[count], 1, count + 1);
                    BodyContent.Children.Add(AllPriceLabel[count], 2, count + 1);
                    BodyContent.Children.Add(AllQuantityLabel[count], 3, count + 1);
                    BodyContent.Children.Add(AllSubTotalLabel[count], 4, count + 1);
                    BodyContent.Children.Add(ButtonLayout[count], 5, count + 1);
                }

                //Insert total price into table
                BodyContent.Children.Add(new Label { HorizontalTextAlignment = TextAlignment.End, Text = $"Total Price : RM {TotalPrice:F2}" }, 0, 5, order_length + 1, order_length + 2);

                Button CheckOutButton = new Button
                {
                    BackgroundColor = Color.FromHex("#03a1fc"),
                    FontSize = 20,
                    TextColor = Color.White,
                    Text = "CheckOut"
                };
                CheckOutButton.Clicked += CheckOutButtonClicked;

                Content = new StackLayout
                {
                    Children = { new ScrollView { Content = BodyContent }, CheckOutButton }
                };
            }
        }

        //Return SELECT * FROM OrderModel WHERE Status = "Pending"
        public List<OrderModel> GetPendingOrderList()
        {
            return Database.db.Table<OrderModel>().Where(Model => Model.Status == "Pending").ToList();
        }

        private void EditButtonClicked(object sender, EventArgs e)
        {
            for(count = 0; count < order_length; count++)
            {
                if(sender == EditButton[count])
                {
                    break;
                }
            }

            Navigation.PushAsync(new EditOrder(OrderList[count]));
        }

        private async void DeleteButtonClicked(object sender, EventArgs e)
        {
            for(count = 0; count < order_length; count++)
            {
                if(sender == DeleteButton[count])
                {
                    break;
                }
            }

            //If user click yes, will remove item and refresh content
            if(await DisplayAlert("Delete Order", $"Are you sure want to remove {OrderList[count].Name} from list?", "YES", "NO"))
            {
                int OrderID = OrderList[count].Id;
                Database.db.Table<OrderModel>().Delete(Model => Model.Id == OrderID);

                RefreshContent();
            }
        }

        private async void CheckOutButtonClicked(object sender, EventArgs e)
        {
            for(count=0; count < order_length; count++)
            {
                OrderList[count].Status = "Confirmed";
            }
            Database.db.UpdateAll(OrderList);
            await DisplayAlert("Order Successfully", "Your order have successfully added.\nWe will serve it to you soon.", "OK");
            await Navigation.PopToRootAsync();
        }
    }
}