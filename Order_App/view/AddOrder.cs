﻿using Order_App.model;
using System;
using Xamarin.Forms;

namespace Order_App.view
{
    public class AddOrder : ContentPage
    {
        //Declaration of View
        private Button BackButton, AddButton;
        private Label QuantityLabel, SubTotalLabel;
        private Stepper QuantityStepper;

        //Model
        private OrderModel OrderObj = new OrderModel();

        public AddOrder(string name, double price)
        {
            OrderObj.Name = name;
            OrderObj.Price = price;

            Label NameLabel = new Label
            {
                FontSize = 30,
                VerticalOptions = LayoutOptions.Center,
                Text = $"Name : {name}"
            };

            Label PriceLabel = new Label
            {
                FontSize = 30,
                VerticalOptions = LayoutOptions.Center,
                Text = $"Price : RM {price:F2}"
            };

            QuantityLabel = new Label
            {
                FontSize = 30,
                VerticalOptions = LayoutOptions.Center,
                Text = "Quantity : 1"
            };

            QuantityStepper = new Stepper
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Minimum = 1,
                Value = 1
            };

            SubTotalLabel = new Label
            {
                FontSize = 30,
                VerticalOptions = LayoutOptions.Center,
                Text = $"SubTotal : RM {price:F2}"
            };

            BackButton = new Button
            {
                BackgroundColor = Color.FromHex("#03a1fc"),
                FontSize = 20,
                TextColor = Color.White,
                Text = "Back To Menu",
            };

            AddButton = new Button
            {
                BackgroundColor = Color.FromHex("#03a1fc"),
                FontSize = 20,
                TextColor = Color.White,
                Text = "Add Order",
            };

            Grid BodyContent = new Grid
            {
                ColumnDefinitions =
                {
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition{ Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition{ Height = new GridLength(70, GridUnitType.Absolute) },
                    new RowDefinition{ Height = new GridLength(70, GridUnitType.Absolute) },
                    new RowDefinition{ Height = new GridLength(70, GridUnitType.Absolute) },
                    new RowDefinition{ Height = new GridLength(70, GridUnitType.Absolute) },
                    new RowDefinition{ Height = GridLength.Auto }
                }
            };

            BodyContent.Children.Add(NameLabel, 0, 2, 0, 1);
            BodyContent.Children.Add(PriceLabel, 0, 2, 1, 2);
            BodyContent.Children.Add(QuantityLabel, 0, 2);
            BodyContent.Children.Add(QuantityStepper, 1, 2);
            BodyContent.Children.Add(SubTotalLabel, 0, 2, 3, 4);
            BodyContent.Children.Add(BackButton, 0, 4);
            BodyContent.Children.Add(AddButton, 1, 4);

            //Event Handling
            QuantityStepper.ValueChanged += StepperClicked;
            BackButton.Clicked += ButtonClicked;
            AddButton.Clicked += ButtonClicked;

            //Set Page
            Title = "Order App";
            Padding = 10;
            Content = BodyContent;
        }

        private void StepperClicked(object sender, ValueChangedEventArgs evt)
        {
            QuantityLabel.Text = "Quantity : " + evt.NewValue;
            SubTotalLabel.Text = $"SubTotal : RM {OrderObj.Price * evt.NewValue:F2}"; 
        }
        private void ButtonClicked(object sender, EventArgs e)
        {
            if(sender == BackButton)
            {
                Navigation.PopAsync();
            }
            else if(sender == AddButton)
            {
                OrderObj.Quantity = Convert.ToInt32(QuantityStepper.Value);
                OrderObj.Subtotal = OrderObj.Price * OrderObj.Quantity;
                OrderObj.Status = "Pending";

                Database.db.CreateTable<OrderModel>();
                Database.db.Insert(OrderObj);

                Navigation.PopToRootAsync();
            }
        }
    }
}